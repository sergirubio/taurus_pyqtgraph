API
===================
.. toctree::
   :maxdepth: 5

.. automodule:: taurus_pyqtgraph.autopantool
   :members:
   :special-members:
   :show-inheritance:

.. automodule:: taurus_pyqtgraph.buffersizetool
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.cli
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.curveproperties
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.curvesmodel
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.curvespropertiestool
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.datainspectortool
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.dateaxisitem
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.forcedreadtool
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.legendtool
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.plot
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.taurusimageitem
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.taurusmodelchoosertool
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.taurusplotdataitem
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.taurustrendset
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.trend
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.util
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.y2axis
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.exporters
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.statisticstool
   :members:
   :show-inheritance:
   :special-members:

.. automodule:: taurus_pyqtgraph.titlepatterneditor
   :members:
   :show-inheritance:
   :special-members:
